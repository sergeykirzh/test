FROM  centos:7

RUN mkdir -p /python_api
COPY python-api.py /python_api/
WORKDIR /usr/src/


RUN yum -y install gcc \
    openssl-devel \
    bzip2-devel \
    libffi-devel \
    zlib-devel \
    xz-devel \
    make  > /dev/null \
    && yum clean all \
    && curl https://www.python.org/ftp/python/3.7.11/Python-3.7.11.tgz  -O \
    && tar xzf Python-3.7.11.tgz  \
    && rm -f Python-3.7.11.tgz  \
    && cd Python-3.7.11 \
    && ./configure --enable-optimizations  > /dev/null \
    && make altinstall  > /dev/null \
    && pip3.7 install --no-cache-dir flask flask-jsonpify flask-restful >/dev/null

WORKDIR /python_api/
EXPOSE 5290

CMD ["python3.7", "python-api.py"]

